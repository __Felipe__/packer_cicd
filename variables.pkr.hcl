variable "profile" {
  type    = string
  default = null
}

variable "instance_profile" {
  type    = string
}

variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "instance_type" {
  type    = string
}

variable "account_id" {
  type    = string
}

variable "vpc_id" {
  type    = string
}

variable "subnet_id" {
  type    = string
}

variable "ami_name"{
    type  = string
}

variable "env" {
  type = string
}

variable "filter" {
  type    = object({
    virtualization = string
    regex = string
    device = string
    owners = list(string)
  })
}

locals {
  tags = {
    "cost:business-unit" = "REVOLVE"
    "cost:environment" = var.env
    "cost:component" = "AMI"
  }
  run_tags = {
    "cost:business-unit" = "REVOLVE"
    "cost:environment" = var.env
    "cost:component" = "EBS"
  }
}
