build {
  sources = ["source.amazon-ebs.demo"]

  provisioner "shell" {
    pause_before = "10s"
    inline = [
  # // packages update
  #      "sudo dnf update -y",
  // install Apache server 
       "sudo dnf install httpd -y",
       "sudo systemctl enable httpd",
       "sudo systemctl start httpd",  
       "sudo chmod 777 /var/www/html/"
    ]
  }
  // copy index.html for dispaying a web page
    provisioner "file" {        
      source = "./src/index.html"
      destination = "/var/www/html/"
  }
    provisioner "shell" {
      inline = [
  // set default permission on /var/www/html/ folder      
        "sudo chmod 755 /var/www/html/",
  // AWS SSM agent installation
        "cd /tmp",
        "sudo dnf install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm",
        "sudo systemctl enable amazon-ssm-agent",
        "sudo systemctl start amazon-ssm-agent"
    ]
  }
}


