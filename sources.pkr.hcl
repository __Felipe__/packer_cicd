source "amazon-ebs" "demo" {
    assume_role {
    role_arn     = "arn:aws:iam::${var.account_id}:role/Packer_Instance_Profile"
    session_name = "Demo"
    external_id  = "Unique_ID_for_Packer_Demo"
    }
    communicator = "ssh"
    ssh_port = "22"
    ssh_username = "ec2-user"
    iam_instance_profile = var.instance_profile

    profile = var.profile
    region = var.region

    instance_type = var.instance_type
    vpc_id = var.vpc_id
    subnet_id = var.subnet_id
    temporary_security_group_source_cidrs = ["0.0.0.0/0"]

    ami_name= "${var.ami_name}-${var.env}-{{isotime \"2006-01-02 03-04-05\"}}"

    source_ami_filter {
        filters = {
            virtualization-type = var.filter["virtualization"]
            name = var.filter["regex"]
            root-device-type = var.filter["device"]
        }
        owners = var.filter["owners"]
        most_recent = true
    }

    launch_block_device_mappings {
        device_name = "/dev/sda1"
        encrypted = false
        delete_on_termination = true
        volume_type = "gp2"
        volume_size = 20
    }

    run_tags = merge(local.run_tags,
        {
            "Name" = "packer-build-${lower(var.ami_name)}-${lower(var.env)}-{{isotime \"2006-01-02-15-04-05\"}}"
        }
    )

    tags = merge(local.tags,
        {
            Name = "${var.ami_name}-{{isotime \"2006-01-02 03-04-05\"}}"
            OS_Version = "RHEL8"
            Release = "Latest"
            Base_AMI_Name = "{{ .SourceAMIName}}"
        }
    )
}
